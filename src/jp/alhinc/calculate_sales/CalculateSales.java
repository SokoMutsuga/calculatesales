package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	//定義
	private static final String BRANCH = "支店定義";
	private static final String COMMODITY = "商品定義";
	private static final String BRANCH_REGEX = "[0-9]{3}";
	private static final String COMMODITY_REGEX = "^[A-Za-z0-9]{8}";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "ファイルのフォーマットが不正です";
	private static final String FILE_NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String NUMBER_OVER_TEN = "合計⾦額が10桁を超えました";
	private static final String SOLD_FILE_INVALID_FORMAT = "のフォーマットが不正です";
	private static final String CODE_FILE_INVALID_FORMAT = "の支店コードが不正です";
	private static final String COMMODITY_CODE_FILE_INVALID_FORMAT = "の商品コードが不正です";


	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();

		//コマンドライン引数が渡されているか確認する
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);

			return;
		}

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, BRANCH, BRANCH_REGEX )) {
			return;
		}

		//商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, branchSales, COMMODITY, COMMODITY_REGEX)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		for(int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();
			if(files[i].isFile() && fileName.matches("^[0-9]{8}.rcd")) {
				rcdFiles.add(files[i]);
			}
		}

		//売上ファイルが連番か確認する
		Collections.sort(rcdFiles);
		for(int j = 0; j < rcdFiles.size() -1 ; j++) {
			int former = Integer.parseInt(rcdFiles.get(j).getName().substring(0,8));
			int latter =  Integer.parseInt(rcdFiles.get(j + 1).getName().substring(0,8));
			if((latter - former) != 1) {
				System.out.println(FILE_NOT_SERIAL_NUMBER);

				return;
			}
		}

		//forをもう一個作った。63行目のforのi < files.lengthだと範囲外とエラーが出るため）
		for(int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;
			try{
				File file = rcdFiles.get(i);
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				ArrayList<String> lines = new ArrayList<String>();

				String line;
				while((line = br.readLine()) != null) {
					lines.add(line);
				}

				//売上ファイルのフォーマットを確認する
				if(lines.size() != 3) {
					System.out.println(rcdFiles.get(i) + SOLD_FILE_INVALID_FORMAT);

					return;
				}

				//Mapに特定のKeyが存在するか確認する
				if(!branchNames.containsKey(lines.get(0)) ){
					System.out.println(rcdFiles.get(i) + CODE_FILE_INVALID_FORMAT);

					return;
				} else if(!commodityNames.containsKey(lines.get(1))) {
					System.out.println(rcdFiles.get(i) + COMMODITY_CODE_FILE_INVALID_FORMAT);

					return;
				}


				//売上金額が数字なのかを確認する
				if(!lines.get(2).matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);

					return;
				}

				long fileSale = Long.parseLong(lines.get(2));
				Long saleAmount = branchSales.get(lines.get(0)) + fileSale;
				Long commodityAmount = branchSales.get(lines.get(1)) + fileSale;

				//売上⾦額の合計が10桁を超えたか確認する
				if((saleAmount >= 1000000000L) || (commodityAmount >= 1000000000L)) {
					System.out.println(NUMBER_OVER_TEN);

					return;
				}

				branchSales.put(lines.get(0),saleAmount);
				branchSales.put(lines.get(1),commodityAmount);
				//System.out.println(branchNames.get(lines.get(0)) + "入力値" + fileSale);
				//System.out.println(branchNames.get(lines.get(0)) + "合計値"	+ "" + saleAmount);
				//System.out.println(commodityNames.get(lines.get(1)) + "商品合計値"	+ "" + commodityAmount);
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);

			} finally{
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);

					}
				}
			}
		}


		//支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		//商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, branchSales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales, String typeName, String regex ) {

		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			//定義ファイルの存在を確認する
			if(!file.exists()) {
				System.out.println(typeName + FILE_NOT_EXIST);

				return false;
			}



			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				//items[0]は支店コード　items [1]は支店名
				String[] items = line.split(",");

				//定義ファイルのフォーマット確認
				if((items.length != 2) || (!items[0].matches(regex))) {
					System.out.println(typeName + FILE_INVALID_FORMAT);

					return false;
				}


				branchNames.put(items[0],items[1]);
				branchSales.put(items[0],0L);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);

					return false;
				}
			}
		}

		return true;
	}


	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File(path,fileName);
			FileWriter fr = new FileWriter(file);
			bw = new BufferedWriter(fr);

			for(String key: branchNames.keySet()) {
				bw.write(key + "," + branchNames.get(key) +  "," + branchSales.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);

			return false;

		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);

					return false;
				}
			}
		}

		return true;
	}

}
